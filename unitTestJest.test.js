const R = require('ramda')

const gcd = function(a, b) {
    if (b === 0) {
        return a;
    }

    return gcd(b, a % b);
};

function ratio(a, b) {
    let calculated = gcd(a, b)
    return `${a/calculated}:${b/calculated}`
}
test('give the gcd of 8 and 6 to equal 2', () => {
    expect(gcd(8, 6)).toBe(2);
});

test('give the ratio of 8 and 6 to equal 4:3', () => {
    expect(ratio(8, 6)).toBe('4:3');
});


let arr = [{
        message: "hey",
        likes: 5
    },
    {
        message: "hey1",
        likes: 14
    },
    {
        message: "hey2",
        likes: 6
    }
]

const totallikes = (array) => array.reduce((total, tweet) => total + tweet.likes, 0)

test('give the totallikes of the array "arr" to equal 25', () => {
    expect(totallikes(arr)).toBe(25);
});

let users = [{ name: 'Sean', followers: 5 }, { name: 'Dave', followers: 57 }, { name: 'George', followers: 25 }]

const sortbyFollower = R.sort((user1, user2) => user2.followers - user1.followers)


let getTopUser = R.pipe(
    sortbyFollower,
    R.take(1)

)

function name(array) {
    return array[0].name
}

test('give the users array return the user with the most followers to equal "Dave". ', () => {
    expect(name(getTopUser(users))).toBe("Dave");
});

const tweet = [{
    time: 4,
    message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \nNulla maximus, magna eget luctus maximus, \ntortor ex fermentum massa, sit amet.",
    likes: 123,
}, {
    time: 4,
    message: "Hey Hey good morninggg",
    likes: 231,
}, {
    time: 3,
    message: "DeHey",
    likes: 23,
}, ]

const mostlikedTweet = (tweet1, tweet2) => {
    const comparedLikes = Math.max(tweet1.likes, tweet2.likes)
    if (comparedLikes === tweet1.likes) {
        return tweet1
    } else {
        return tweet2
    }

}

function mostlikedAnalytics(array) {

    const mostliked = array.reduce(mostlikedTweet)
    return mostliked.message

}

test('give the tweet array return the most liked tweet ', () => {
    expect(mostlikedAnalytics(tweet)).toBe("Hey Hey good morninggg");
});