class Usertweet{
    constructor(user){
        this.user = user
    }

    notify(tweet){
        console.log(`${this.user}'s tweet: ${tweet}`)
    }
}

module.exports = Usertweet
