____TODO____
1 feature with composition
4 pure functions

add to followers and to following //Done
add like tweet functions //DONE
add check followers and following function //Done

Rules
no-arguments - Forbid the use of arguments.
no-class - Forbid the use of class.
no-delete - Forbid the use of delete.
no-events - Forbid the use of the events module.
no-get-set - Forbid the use of getters and setters.
no-let - Forbid the use of let.
no-loops - Forbid the use of loops.
no-mutating-assign - Forbid the use of Object.assign() with a variable as first argument.
no-mutating-methods - Forbid the use of mutating methods.
no-mutation - Forbid the use of mutating operators.
no-nil - Forbid the use of null and undefined.
no-proxy - Forbid the use of Proxy.
no-rest-parameters - Forbid the use of rest parameters.
no-this - Forbid the use of this.
no-throw - Forbid the use of throw.
no-unused-expression - Enforce that an expression gets used.
no-valueof-field - Forbid the creation of valueOf fields.

reference: https://github.com/jfmengels/eslint-plugin-fp

not enforced strictly pero try lang para goods ta sa FP