const User = require('./User')
const Follow = require('./Follow')

const batman = new Follow("Batman")
const wolverine = new Follow("Logan")
const hulk = new Follow("Bruce")
const greenlanter = new Follow("Benedict")
const superman = new Follow("Superman")

const sean = new User('Sean')

batman.subscribe(sean)
wolverine.subscribe(sean)
hulk.subscribe(sean)
greenlanter.subscribe(sean)
superman.subscribe(sean)

batman.follow()
wolverine.follow()
hulk.follow()
greenlanter.follow()
superman.follow()