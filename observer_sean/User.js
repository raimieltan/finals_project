class Users {
    constructor(name) {
        this.name = name
        this.followers = []
    }
    addtoFollowers(followerName) {
        this.followers.push(followerName)
    }
    notify(followerName) {
        console.log(`${followerName} followed you`)
        console.log(`Followers: ${this.followers}`)
    }
}

module.exports = Users