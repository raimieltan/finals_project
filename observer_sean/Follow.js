class Follow {
    constructor(name) {
        this.name = name;
        this.subscriber = []
    }
    subscribe(observer) {
        this.subscriber.push(observer)
    }
    follow() {
        this.subscriber.forEach(observer => {
            observer.addtoFollowers(this.name)
            observer.notify(this.name)
            console.log("____________")
        })
    }
}

module.exports = Follow