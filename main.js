const readline = require(`readline-sync`)
const chalk = require('chalk');
const accounts = require('./accounts.js')
const R = require('ramda')

const d = new Date()
let signedin = 0
let canlogin = true

const gcd = function(value1, value2) {
    if (value2 === 0) {
        return value1;
    }

    return gcd(value2, value1 % value2);
};

function ratio(value1, value2) {
    let calculated = gcd(value1, value2)
    return `${value1/calculated}:${value2/calculated}`
}

function followAnalytics() {
    const displayFollows = (array) => {
        return array
    }

    const followers = displayFollows(accounts[signedin].followers)
    const following = displayFollows(accounts[signedin].following)
    console.log("Followers: " + followers)
    console.log("Followers: " + following)
}

function mostlikedAnalytics() {
    const mostlikedTweet = (tweet1, tweet2) => {
        const comparedLikes = Math.max(tweet1.likes, tweet2.likes)
        if (comparedLikes === tweet1.likes) {
            return tweet1
        } else {
            return tweet2
        }

    }
    const mostliked = accounts[signedin].tweet.reduce(mostlikedTweet)
    console.log(`Your most liked tweet is: "${mostliked.message}" with a total of ${mostliked.likes} likes`)

}

function topUserAnalytics() {
    const sortbyFollower = R.sort((user1, user2) => user2.followers.length - user1.followers.length)


    let getTopUser = R.pipe(
        sortbyFollower,
        R.take(1)
    )
    console.log(`The user with the most followers is: ${getTopUser(accounts)[0].fullName}-${getTopUser(accounts)[0].username}`)
}

function totalLikesAnalytics() {
    const totallikes = (array) => array.reduce((total, tweet) => total + tweet.likes, 0)

    console.log(`Your total like count is: ${totallikes(accounts[signedin].tweet)}`)
}
const userprofile = {
    name: "User Profie",
    method: () => {
        console.clear()
        console.log("_USER PROFILE_")
        let user = accounts[signedin]
        console.log(user.fullName)
        console.log(user.username)

        console.log(`${user.following.length} Following ${user.followers.length} Followers`)
        console.log(`Follwing to follower ratio: ${ratio(user.following.length, user.followers.length)}`)
        console.log("TWEETS")
        console.log("____________________________")
        user.tweet.forEach((x) => {
            console.log(`${x.message}`)
            console.log(`♥ : ${x.likes} - ${ d.getHours()-x.time}h ago`)
            console.log("____________________________")
        })
        usermenu(addtweet, feed, followSomeone, exit, analytics)
    }

}

const followSomeone = {
    name: "Follow",
    method: () => {
        const search = readline.question("Enter the username: ")
        const userindex = (accounts.findIndex((users) => users.username == search))

        function follow(a, b) {
            console.log(accounts[b].username)
            if (accounts[a].following.includes(accounts[b].username)) {
                console.log("You are already following this user")
            } else {
                accounts[a].following.push(accounts[b].username)
                accounts[b].followers.push(accounts[a].username)
            }



        }
        follow(signedin, userindex)
        userprofile.method()

    }
}
const likeTweet = {
    name: "Like tweet",
    method: () => {
        const search = readline.question("Enter the username: ")
        const userindex = (accounts.findIndex((users) => users.username == search))

        accounts[userindex].tweet.forEach((tweets) => {
            console.log(`${accounts[userindex].tweet.indexOf(tweets)} - ${tweets.message}`)
        })

        const tweetindex = readline.question("Enter chosen tweet: ")


        function addtolikes(a, b) {
            accounts[a].tweet[b].likes++
        }

        addtolikes(userindex, tweetindex)
        console.log(`Liked, this tweet now has ${accounts[userindex].tweet[tweetindex].likes}`)
        mainmenu(userprofile, feed, likeTweet)

    }
}

const addtweet = {
    name: "Add Tweet",
    method: () => {
        let tweet = readline.question("Enter tweet: ")
        accounts[signedin].tweet.push({
            time: d.getHours(),
            message: tweet,
            likes: 0
        })
        mainmenu(userprofile, feed, likeTweet)
    }

}

const exit = {
    name: "Sign out",
    method: () => {
        console.log("Signing Out")
        mainmenu(login, signup, homepage)
    }
}

const feed = {
    name: "Display Feed",
    method: () => {
        console.clear()
        console.log('_FEED_')
        accounts.forEach((account) => {

            let sorted = R.sort((value1, value2) => ((b.time - d.getHours()) - (value1.time - d.getHours())) ? 1 : -1, account.tweet)

            console.log(`${account.fullName} ${account.username}- ${ d.getHours()-sorted[0].time}h`)
            console.log(sorted[0].message)

            console.log("♥ : " + sorted[0].likes)
            console.log("")


        })
        mainmenu(userprofile, addtweet, likeTweet)
    }
}

function mainmenu(value1, value2, value3) {
    let choice = readline.question(`\t\t1. ${value1.name} \n\t\t2. ${value2.name} \n\t\t3. ${value3.name}\n>`)
    if (choice == 1) {
        value1.method()
    } else if (choice == 2) {
        value2.method()
    } else {
        value3.method()
    }
}

function usermenu(value1, value2, value3,value4,value5) {
    let choice = readline.question(`\t\t1. ${value1.name} \n\t\t2. ${value2.name} \n\t\t3. ${value3.name} \n\t\t4. ${value4.name} \n\t\t5. ${value5.name}\n>`)
    if (choice == 1) {
        value1.method()
    } else if (choice == 2) {
        value2.method()
    } else if(choice === 3){
        value3.method()
    } else if(choice === 4){
        value4.method()
    }else{
        value5.method()
    }
}

const analytics = {
    name: "Analytics",
    method: () => {
        console.log("ANALYTICS")
        totalLikesAnalytics()
        followAnalytics()
        mostlikedAnalytics()
        topUserAnalytics()
        const stopper = readline.question()
        userprofile.method()

    }
}

const login = {
    name: "Login",
    method: () => {
        console.log(chalk.blue("LOGIN"))
        let username = String(readline.question(`Username: `))
        let password = String(readline.question(`Password: `, {
            hideEchoBack: true,
            mask: " "
        }));


        accounts.forEach(x => {
            if (x.username === username && x.password === password) {
                signedin = accounts.indexOf(x)
                feed.method()
            } else {
                canlogin = false
            }
        })

        if (canlogin == false) {
            console.log(chalk.red("No user under that!"))
            mainmenu(login, signup, homepage)
            canlogin = true
        }
    }
}

const signup = {
    name: "Sign Up",
    method: () => {
        let canpush = true
        let fullName = readline.question("Enter full name: ")

        function suggestedUsernames() {
            const randomewords = ["master18", "awesome", "brainy.", "angels", "appealing"]
            const splitter = R.split(' ')
            const mapper = R.map((value) => value + randomewords[Math.floor(Math.random() * ((randomewords.length) - 0) + 0)])


            let generateUsernames = R.pipe(
                splitter,
                mapper
            )
            console.log(`Suggestions:\n\t${generateUsernames(fullName)}`)
        }
        suggestedUsernames()
        let username = readline.question("Enter username: ")
        let password = readline.question("Enter password: ")
        let tweet = readline.question("Let us hear your first tweet: ")


        for (let x of accounts) {
            if (x.username === username) {
                console.log("Account exists")
                canpush = false;
                break;
            }
        }

        if (canpush) {
            accounts.push({
                fullName: fullName,
                username: username,
                password: password,
                following: [],
                followers: ["@freefollower"],
                tweet: [{
                    time: d.getHours(),
                    message: tweet,
                    likes: 0,
                }]
            })
        }

        login.method()
    }
}

const homepage = {
    name: "About",
    method: () => {
        console.log("This is a social media that lets users post short messages or status")
        console.log("Twitter knockoff")
        mainmenu(login, signup, homepage)
    }
}

mainmenu(login, signup, homepage)